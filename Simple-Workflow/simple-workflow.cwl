cwlVersion: v1.0
class: Workflow
requirements:
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement

doc: |
  Workflow of 2 steps

inputs:

  read1:
    type: File
    doc: files containing the paired/single-end reads in fastq format

  read2:
    type: File
    doc: files containing the paired/single-end reads in fastq format

  sample:
    type: string

  rg_name:
    type: string?
    doc: Flowcell code.1

  seq_center:
    type: string?
    doc: Sequencing Center

steps:

  fastq-to-sam:
    run: https://gitlab.com/precmed/pipelines/demo/raw/master/Tool/picard-fastq-to-sam.cwl
    in:
        read1: read1
        read2: read2
        ubam_name: sample
        seq_center: seq_center
        rg_name: rg_name
    out: [ubam]

  mark-illumina-adapters:
    run: ./tools/picard/picard-mark-illumina-adapters.cwl
    in:
        ubam: fastq-to-sam/ubam
        ma_bam_name: sample
    out: [ma_bam]

  
outputs:
  ubam:
    type: File
    outputSource: fastq-to-sam/ubam

  ma_bam:
    type: File
    outputSource: mark-illumina-adapters/ma_bam
