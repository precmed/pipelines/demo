cwlVersion: v1.0
class: CommandLineTool

requirements:
   - class: InlineJavascriptRequirement
   - class: DockerRequirement
     dockerPull: "broadinstitute/picard"

baseCommand: "/usr/picard/docker_helper.sh"

 
inputs:

    ubam:
        type: File
        inputBinding:
              position: 3
              separate: false
              prefix: I=

    ma_bam_name:
        type: string
        inputBinding:
              position: 4
              valueFrom: $(self.concat(".MA.bam"))
              separate: false
              prefix: O=

outputs:
    ma_bam:
        type: File
        outputBinding:
            glob: $(inputs.ma_bam_name.concat(".MA.bam"))

arguments:

  - valueFrom: "-Xmx8G"
    position: 1
    separate: true
    prefix: -j

  - valueFrom: "MarkIlluminaAdapters"
    position: 2


  - valueFrom: $(inputs.ma_bam_name.concat("_markadapt.txt"))
    position: 5
    separate: false
    prefix: M=
