cwlVersion: v1.0
class: CommandLineTool

requirements:
   - class: InlineJavascriptRequirement
   - class: DockerRequirement
     dockerPull: "broadinstitute/picard"
   - class: ResourceRequirement
     tmpdirMin: 5000
     outdirMin: 3000

baseCommand: "/usr/picard/docker_helper.sh"

inputs:

    read1:
        type: File
        inputBinding:
            position: 3
            separate: false
            prefix: FASTQ=

    read2:
        type: File
        inputBinding:
              position: 4
              separate: false
              prefix: FASTQ2=

    ubam_name:
        type: string
        inputBinding:
              position: 5
              valueFrom: $(self.concat(".ubam.bam"))
              separate: false
              prefix: OUTPUT=


    rg_name:
        type: string
        inputBinding:
              position: 6
              separate: false
              prefix: READ_GROUP_NAME=

    seq_center:
        type: string
        inputBinding:
              position: 11
              separate: false
              prefix: SEQUENCING_CENTER=
outputs:
    ubam:
        type: File
        outputBinding:
            glob: $(inputs.ubam_name.concat(".ubam.bam"))


arguments:

  - valueFrom: "-Xmx8G"
    position: 1
    separate: true
    prefix: -j

  - valueFrom: "FastqToSam"
    position: 2

  - valueFrom: $(inputs.read1.basename)
    position: 7
    separate: false
    prefix: SAMPLE_NAME=

  - valueFrom: "A"
    position: 8
    separate: false
    prefix: LIBRARY_NAME=

  - valueFrom: "$(inputs.rg_name)"
    position: 9
    separate: false
    prefix: PLATFORM_UNIT=

  - valueFrom: "illumina"
    position: 10
    separate: false
    prefix: PLATFORM=
