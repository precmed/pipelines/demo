cwlVersion: v1.0
class: CommandLineTool

hints:
  DockerRequirement:
    dockerPull: kelkost/echo-message:latest

baseCommand: ["bash", "/usr/local/src/myscripts/docker_entry.sh", "print"]

inputs:
  message:
    type: string
    inputBinding:
      position: 2

outputs:
  output:
    type: stdout

arguments:

# By enabling this argument the input string will be printed reversed
  - valueFrom: "-r"
    position: 1

stdout: output.txt

