#!/bin/bash

set -e

if [ "$1" = 'print' ]; then

	if [ "$2" = "-r" ]; then
		args+="${@:3}"
		echo $args|rev
	else
		echo "${@:2}"
	fi
else
	echo "No such command"
fi


