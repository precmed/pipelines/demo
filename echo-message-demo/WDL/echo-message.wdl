task Echo {
  String addressee  
  command {
    print "${message}!"  
  }
  output {
    String message = read_string(stdout())
  }
  runtime {
    docker: "kelkost/echo-message:latest"
	#docker: "registry.gitlab.com/precmed/pipelines/demo/echo-message:latest"
  }
}

workflow wf_Echo {
  call Echo

  output {
     Echo.message
  }
}
